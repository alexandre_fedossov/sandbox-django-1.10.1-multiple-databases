import uuid as uuid_package

from django.db import models


class ItemA(models.Model):
    uuid = models.UUIDField(default=uuid_package.uuid4)
    uuid2 = models.UUIDField(default=uuid_package.uuid4)


class ItemB(models.Model):
    uuid = models.UUIDField(default=uuid_package.uuid4)
    uuid2 = models.UUIDField(default=uuid_package.uuid4)


# noinspection PyUnusedLocal, PyProtectedMember
class Router(object):
    """
    A router to control all database operations on models in the application.
    """

    @staticmethod
    def db_for_read(model, **hints):
        """
        Attempts to read models.
        """
        return 'another' if model._meta.app_label == 'another' else None

    @staticmethod
    def db_for_write(model, **hints):
        """
        Attempts to write models.
        """
        return 'another' if model._meta.app_label == 'another' else None

    @staticmethod
    def allow_relation(obj1, obj2, **hints):
        """
        Allow relations if a model in the app is involved.
        """
        return True if obj1._meta.app_label == 'another' or obj2._meta.app_label == 'another' else None

    @staticmethod
    def allow_migrate(db, app_label, model_name=None, **hints):
        """
        Make sure the app only appears in the database.
        """
        return db == 'another' if app_label == 'another' else None
