# Django 1.10.1 Multiple Databases Example


## Installation

* `git clone
  https://alexandr_fedosov@bitbucket.org/alexandr_fedosov/sandbox-django-1.10.1-multiple-databases.git`
* `mkvirtualenv django-1.10-multiple-databases`
* `pip install -r requirements.txt`

## Migrations

### Initial and normal process

* `python manage.py migrate`
* `python manage.py migrate another --database another`

### Create migrations

As always:

* `python manage.py makemigrations`

## Result

![databases.png](https://bitbucket.org/repo/Kkreg6/images/3654289851-databases.png)